# Documentação - Loja da Victoria

O programa é baseado principalmente em um menu principal e outros 3 submenus (Venda, Recomendação e Estoque), **dentro do Menu principal e do Menu do Modo Estoque não é necessário pressionar enter** apos digitar o comando desejado.

## Inicialização
Para inicializar o programa basta utilizar o makefile dentro da pasta raiz da maneira indicada:  
1. make clean
2. make
3. make run  

Note que assim que o programa é inicializado será exibida uma mensagem em ASCII art por cerca de 4 segundos na tela

## Arquivos .txt
Os arquivos de dados que serão manipulados pelo programa (que ficam dentro da pasta data) não são necessários tendo em vista que é possível criar novos arquivos dentro do Modo Estoque do programa, eles estão presentes apenas pela conveniência de não precisar criar nenhum arquivo antes de se começar a usar qualquer modo do programa, **entretanto os diretórios 'clientes' e 'produtos' dentro da pasta data são de vital importância para o funcionamento do programa**

## Inputs
Antes de cada input do usuário são dadas claras instruções de como fornecer a entrada, muitas vezes com exemplos.