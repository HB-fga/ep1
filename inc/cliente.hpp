#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include "metodos.hpp"
#include "produto.hpp"
#include "cliente.hpp"

#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
#include <vector>

class Cliente {
protected:
    std::string CPF;
    std::string nome;
    std::vector <int> historico;
public:
    Cliente();
    ~Cliente();
    std::string get_CPF();
    void set_CPF(std::string CPF);
    std::string get_nome();
    void set_nome(std::string nome);
    std::vector <int> get_historico();
    void set_historico(std::vector <int> historico);
    void preencher_dados(std::string CPF_lido);
    virtual int calcula_preco(std::vector <Produto> carrinho); // Polimorfismo - Sobreescrita
    virtual int calcula_desconto(std::vector <Produto> carrinho); // Polimorfismo - Sobreescrita
};

#endif