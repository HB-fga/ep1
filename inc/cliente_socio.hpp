#ifndef CLIENTE_SOCIO_HPP
#define CLIENTE_SOCIO_HPP

#include "metodos.hpp"
#include "produto.hpp"
#include "cliente.hpp"
#include "cliente_socio.hpp"

#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
#include <vector>

class Cliente_Socio : public Cliente {
public:
    Cliente_Socio();
    ~Cliente_Socio();
    int calcula_preco(std::vector <Produto> carrinho); // Polimorfismo - Sobreescrita
    int calcula_desconto(std::vector <Produto> carrinho); // Polimorfismo - Sobreescrita
};

#endif