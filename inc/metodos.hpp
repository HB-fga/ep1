#ifndef METODOS_HPP
#define METODOS_HPP

#include "metodos.hpp"
#include "produto.hpp"
#include "cliente.hpp"
#include "cliente_socio.hpp"

#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
#include <vector>

char input(void);
std::string to_string_preco(int preco);
int linhas(std::string path);
void listar_categorias(void);
std::string lista_de_produtos(void);
void listar_produtos(void);
void listar_venda(void);
int numero_de_produtos(void);
std::vector <int> preencher_estoque(void);
void editar_quantidade(std::string produto, int num);
void editar_historico(std::string cpf, std::vector <int> hist);
std::vector <Produto> recomendar(std::vector <int> historico, std::string acao);
float get_peso_total(std::vector <Produto> carrinho);

#endif