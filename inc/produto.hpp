#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include "metodos.hpp"
#include "produto.hpp"

#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
#include <vector>

class Produto {
private:
    std::string nome;
    std::vector <int> categorias;
    int preco;
    float peso;
    int quantidade;
public:
    Produto();
    Produto(std::string frete); // Polimorfismo - Sobrecarga
    ~Produto();
    std::string get_nome();
    void set_nome(std::string nome);
    std::vector <int> get_categorias();
    void set_categorias(std::vector <int> categorias);
    int get_preco();
    void set_preco(int preco);
    float get_peso();
    void set_peso(float peso);
    int get_quantidade();
    void set_quantidade(int quantidade);
    void preencher_dados(int indice);
};

#endif