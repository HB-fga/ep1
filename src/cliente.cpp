#include "metodos.hpp"
#include "produto.hpp"
#include "cliente.hpp"

#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
#include <vector>

Cliente::Cliente()
{
    nome = "Generico";
    CPF = "00000000000";
    //historico inicializa vazio por padrao
}
Cliente::~Cliente()
{

}
std::string Cliente::get_CPF()
{
    return CPF;
}
void Cliente::set_CPF(std::string CPF)
{
    this->CPF = CPF;
}
std::string Cliente::get_nome()
{
    return nome;
}
void Cliente::set_nome(std::string nome)
{
    this->nome = nome;
}

std::vector <int> Cliente::get_historico()
{
    return historico;
}
void Cliente::set_historico(std::vector <int> historico)
{
    this->historico = historico;
}
void Cliente::preencher_dados(std::string CPF_lido)
{
    std::fstream arquivo;
    std::string linha_arquivo;
    std::string categoria = "";
    int i;
    int ind;

    historico.clear();

    i = linhas("data/categorias.txt");
    while(i--) historico.push_back(0);

    arquivo.open("data/clientes/" + CPF_lido + ".txt", std::ios::in);
    
    if(arquivo.is_open())
    {
        i=0;
        while(!arquivo.eof())
        {
            
            getline(arquivo,linha_arquivo);
            
            if(i == 0) this->CPF = linha_arquivo;
            if(i == 1) this->nome = linha_arquivo;
            if(i == 3)
            {
                ind = 0;
                for(int j=0;j<linha_arquivo.size();j++)
                {
                    if(linha_arquivo.at(j) != ' ')
                    {
                        categoria.insert(categoria.end(),linha_arquivo.at(j));
                    }
                    
                    if(linha_arquivo.at(j) == ' ' || j+1 == linha_arquivo.size())
                    {
                        historico.at(ind) = (stoi(categoria));
                        categoria = "";
                        ind++;
                    }
                }
            }
            if(i >= 3) break;


            i++;
        }

        arquivo.close();
    }
}
int Cliente::calcula_preco(std::vector <Produto> carrinho)
{
    int total=0;

    for(int i=0;i<carrinho.size();i++)
    {
        total += carrinho.at(i).get_preco();
    }

    return total;
}
int Cliente::calcula_desconto(std::vector <Produto> carrinho)
{
    return 0;
}