#include "metodos.hpp"
#include "produto.hpp"
#include "cliente.hpp"
#include "cliente_socio.hpp"

#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
#include <vector>

Cliente_Socio::Cliente_Socio()
{
    nome = "Generico";
    CPF = "00000000000";
    //historico inicializa vazio por padrao
}
Cliente_Socio::~Cliente_Socio()
{
    
}
int Cliente_Socio::calcula_preco(std::vector <Produto> carrinho)
{
    int total=0;

    for(int i=0;i<carrinho.size();i++)
    {
        total += carrinho.at(i).get_preco();
    }

    total *= 85;

    if(total % 100 == 0) total /= 100;
    else total = (total/100) + 1;

    return total;
}
int Cliente_Socio::calcula_desconto(std::vector <Produto> carrinho)
{
    int total = 0;
    int valor_descontado;
    Cliente_Socio cliente_aux;
    valor_descontado = cliente_aux.calcula_preco(carrinho);

    for(int i = 0;i < carrinho.size();i++)
    {
        total += carrinho.at(i).get_preco();
    }

    total -= valor_descontado;

    return total;
}