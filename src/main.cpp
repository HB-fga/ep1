#include "metodos.hpp"
#include "produto.hpp"
#include "cliente.hpp"
#include "cliente_socio.hpp"

#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

int main() {

    system("clear");

    vector <Cliente *> consumidor;
    vector <Produto> carrinho;
    vector <int> estoque;
    vector <int> historico_aux;
    Produto frete("frete");
    fstream arquivo_io;

    string linha_arquivo;
    string string_entrada;
    string cpf_entrada;
    string valor;

    char char_entrada;
    int indice;
    int quantidade;
    int flag_estoque = 0;
    int flag_recomendacao = 0;

    // Mensagem Inicial em ASCII art
    cout<<"           LLLLLLLLLLL                               jjjj                                                                                               \n           L:::::::::L                              j::::j                                                                                              \n           L:::::::::L                               jjjj                                                                                               \n           LL:::::::LL                                                                                                                                  \n             L:::::L                  ooooooooooo  jjjjjjj  aaaaaaaaaaaaa                                                                               \n             L:::::L                oo:::::::::::ooj:::::j  a::::::::::::a                                                                              \n             L:::::L               o:::::::::::::::oj::::j  aaaaaaaaa:::::a                                                                             \n             L:::::L               o:::::ooooo:::::oj::::j           a::::a                 dddddddd                                                    \n             L:::::L               o::::o     o::::oj::::j    aaaaaaa:::::a                 d::::::d                                                    \n             L:::::L               o::::o     o::::oj::::j  aa::::::::::::a                 d::::::d                                                    \n             L:::::L               o::::o     o::::oj::::j a::::aaaa::::::a                 d::::::d                                                    \n             L:::::L         LLLLLLo::::o     o::::oj::::ja::::a    a:::::a                 d:::::d                                                     \n           LL:::::::LLLLLLLLL:::::Lo:::::ooooo:::::oj::::ja::::a    a:::::a         ddddddddd:::::d   aaaaaaaaaaaaa                                     \n           L::::::::::::::::::::::Lo:::::::::::::::oj::::ja:::::aaaa::::::a       dd::::::::::::::d   a::::::::::::a                                    \n           L::::::::::::::::::::::L oo:::::::::::oo j::::j a::::::::::aa:::a     d::::::::::::::::d   aaaaaaaaa:::::a                                   \n           LLLLLLLLLLLLLLLLLLLLLLLL   ooooooooooo   j::::j  aaaaaaaaaa  aaaa    d:::::::ddddd:::::d            a::::a                                   \n                                                    j::::j                      d::::::d    d:::::d     aaaaaaa:::::a                                   \n                                          jjjj      j::::j                      d:::::d     d:::::d   aa::::::::::::a                                   \n                                         j::::jj   j:::::j                      d:::::d     d:::::d  a::::aaaa::::::a                                   \n                                         j::::::jjj::::::j                      d:::::d     d:::::d a::::a    a:::::a                                   \n                                          jj::::::::::::j                       d::::::ddddd::::::dda::::a    a:::::a                                   \n       VVVVVVVV           VVVVVVVV  iiii    jjj::::::jjj               tttt      d:::::::::::::::::da:::::aaaa::::::a      iiii                     !!! \n       V::::::V           V::::::V i::::i      jjjjjj               ttt:::t       d:::::::::ddd::::d a::::::::::aa:::a    i::::i                   !!:!!\n       V::::::V           V::::::V  iiii                            t:::::t        ddddddddd   ddddd  aaaaaaaaaa  aaaa     iiii                    !:::!\n       V::::::V           V::::::V                                  t:::::t                                                                        !:::!\n        V:::::V           V:::::V iiiiiii     ccccccccccccccccttttttt:::::ttttttt       ooooooooooo   rrrrr   rrrrrrrrr  iiiiiii   aaaaaaaaaaaaa   !:::!\n         V:::::V         V:::::V  i:::::i   cc:::::::::::::::ct:::::::::::::::::t     oo:::::::::::oo r::::rrr:::::::::r i:::::i   a::::::::::::a  !:::!\n          V:::::V       V:::::V    i::::i  c:::::::::::::::::ct:::::::::::::::::t    o:::::::::::::::or:::::::::::::::::r i::::i   aaaaaaaaa:::::a !:::!\n           V:::::V     V:::::V     i::::i c:::::::cccccc:::::ctttttt:::::::tttttt    o:::::ooooo:::::orr::::::rrrrr::::::ri::::i            a::::a !:::!\n            V:::::V   V:::::V      i::::i c::::::c     ccccccc      t:::::t          o::::o     o::::o r:::::r     r:::::ri::::i     aaaaaaa:::::a !:::!\n             V:::::V V:::::V       i::::i c:::::c                   t:::::t          o::::o     o::::o r:::::r     rrrrrrri::::i   aa::::::::::::a !:::!\n              V:::::V:::::V        i::::i c:::::c                   t:::::t          o::::o     o::::o r:::::r            i::::i  a::::aaaa::::::a !!:!!\n               V:::::::::V         i::::i c::::::c     ccccccc      t:::::t    tttttto::::o     o::::o r:::::r            i::::i a::::a    a:::::a  !!! \n                V:::::::V         i::::::ic:::::::cccccc:::::c      t::::::tttt:::::to:::::ooooo:::::o r:::::r           i::::::ia::::a    a:::::a      \n                 V:::::V          i::::::i c:::::::::::::::::c      tt::::::::::::::to:::::::::::::::o r:::::r           i::::::ia:::::aaaa::::::a  !!! \n                  V:::V           i::::::i  cc:::::::::::::::c        tt:::::::::::tt oo:::::::::::oo  r:::::r           i::::::i a::::::::::aa:::a!!:!!\n                   VVV            iiiiiiii    cccccccccccccccc          ttttttttttt     ooooooooooo    rrrrrrr           iiiiiiii  aaaaaaaaaa  aaaa !!! \n";
    sleep(4);

    while(1) // Main Loop
    {
        system("clear");

        cout << "MENU PRINCIPAL" << endl << endl;

        cout << "=======================" << endl;
        cout << " [1] Modo Venda        " << endl;
        cout << " [2] Modo Recomendacao " << endl;
        cout << " [3] Modo Estoque      " << endl;
        cout << " [4] Fechar o Programa " << endl;
        cout << "=======================" << endl << endl;

        cout << "Escolha uma opcao: ";
        char_entrada = input();

        if(char_entrada == '3' ) // MODO ESTOQUE
        {
            while(1)
            {
                system("clear");

                cout << "MODO ESTOQUE" << endl << endl;

                cout << "==============================" << endl;
                cout << " [1] Adicionar um Produto     " << endl;
                cout << " [2] Adicionar uma Categoria  " << endl;
                cout << " [3] Ver Produtos             " << endl;
                cout << " [4] Ver Categorias           " << endl;
                cout << " [5] Voltar ao Menu Principal " << endl;
                cout << "==============================" << endl << endl;

                cout << "Escolha uma opcao: ";
                
                char_entrada = input();
                system("clear");

                if(char_entrada == '1') // Adicionar um Produto
                {
                    cout << "Digite o nome do produto\nExemplo: cadeira\n: ";
                    getline(cin,string_entrada);

                    if(linhas("data/produtos/" + string_entrada + ".txt") <= 1) // Produto nao existe
                    {
                        arquivo_io.open("data/produtos/" + string_entrada + ".txt", ios::out | ios::app);

                        if(arquivo_io.is_open())
                        {
                            arquivo_io << string_entrada << endl;
                            
                            cout << "Digite o(s) indice(s) numerico(s) da(s) categoria(s) do produto separados por um espaco em branco\nExemplo: 1 3\n\n";
                            listar_categorias();
                            cout << ": ";
                            getline(cin,string_entrada);
                            arquivo_io << string_entrada << endl;

                            cout << "Digite o preco do produto em Reais\nExemplo: 29.90\n: ";
                            getline(cin,string_entrada);
                            arquivo_io << string_entrada << endl;

                            cout << "Digite o peso do produto em Quilogramas\nExemplo: 7.238\n: ";
                            getline(cin,string_entrada);
                            arquivo_io << string_entrada << endl;

                            cout << "Digite quantas unidades do produto voce deseja inserir no estoque\nExemplo: 7\n: ";
                            getline(cin,string_entrada);
                            arquivo_io << string_entrada << endl;

                            arquivo_io.close();
                        }

                        
                    }
                    else //Produto ja existe
                    {
                        cout << "O produto ja existe no sistema, digite quantas unidades voce deseja adicionar no estoque\nExemplo: 3\n: ";
                        cin >> quantidade;

                        editar_quantidade(string_entrada, quantidade);
                    }
                }
                if(char_entrada == '2') //Adicionar uma Categoria
                {
                    cout << "Digite o nome da Categoria: ";
                    getline(cin,string_entrada);

                    arquivo_io.open("data/categorias.txt", ios::out | ios::app);

                    if(arquivo_io.is_open())
                    {
                        arquivo_io << endl << string_entrada;
                        arquivo_io.close();
                    }

                }
                if(char_entrada == '3') //Ver Produtos
                {
                    cout << "Produtos:" << endl;
                    listar_produtos();
                    cout << "\nPRESSIONE ENTER PARA VOLTAR AO MODO ESTOQUE\n";
                    scanf("%*c");
                }
                if(char_entrada == '4') //Ver Categorias
                {
                    cout << "Categorias:" << endl;
                    listar_categorias();
                    cout << "\nPRESSIONE ENTER PARA VOLTAR AO MODO ESTOQUE\n";
                    scanf("%*c");
                }
                if(char_entrada == '5') //Sair
                {
                    char_entrada = '0';
                    break;
                }
            }
        }
        if(char_entrada == '2' ) // MODO RECOMENDACAO
        {
            system("clear");

            cout << "MODO RECOMENDACAO\n" << endl;
            cout << "Recomendacao de produtos para clientes cadastrados" << endl;
            cout << "Digite o CPF do cliente\nExemplo: 12345678910\n: ";
            cin >> cpf_entrada;
            scanf("%*c");

            if(linhas("data/clientes/" + cpf_entrada + ".txt") <= 1) // Cliente nao Existe
            {
                cout << "O cliente nao esta cadastrado" << endl;
                cout << "\nPRESSIONE ENTER PARA VOLTAR AO MENU PRINCIPAL\n";
                char_entrada = '0';
                scanf("%*c");
            }
            else // Cliente Existe
            {
                consumidor.push_back(new Cliente);

                consumidor.at(0)->preencher_dados(cpf_entrada);

                cout << "Este(s) eh(sao) o(s) produto(s) recomendado(s) exclusivamente para "<< consumidor.at(0)->get_nome() << endl;

                recomendar(consumidor.at(0)->get_historico(), "listar");

                cout << "Deseja comecar uma venda e inserir esses produtos no carrinho?\n[1] Sim, inserir produtos no carrinho do cliente\n[0] Nao, voltar ao menu principal\n: ";

                char_entrada = input();

                if(char_entrada == '1') flag_recomendacao = 1;

                consumidor.pop_back();
            }

        }
        if(char_entrada == '1' ) // MODO VENDA
        {
            system("clear");

            carrinho.clear();
            estoque.clear();

            cout << "MODO VENDA" << endl << endl;

            if(flag_recomendacao == 0)
            {   
                cout << "Digite o CPF do cliente\nExemplo: 12345678910\n: ";
                getline(cin,string_entrada);

                cpf_entrada = string_entrada;
            }
            else
            {
                string_entrada = cpf_entrada;
            }
            

            if(linhas("data/clientes/" + string_entrada + ".txt") <= 1) //Cadastrando o Cliente
            {
                arquivo_io.open("data/clientes/" + string_entrada + ".txt", ios::out | ios::app);

                if(arquivo_io.is_open())
                {
                    arquivo_io << string_entrada << endl;
                    
                    cout << "Digite o nome do cliente\nExemplo: Joao\n: ";
                    getline(cin,string_entrada);
                    arquivo_io << string_entrada << endl;

                    cout << "O cliente deseja se tornar socio?\n[1] Sim, tornar o cliente socio\n[0] Nao, ele sera um cliente comum\n: ";
                    getline(cin,string_entrada);

                    if(string_entrada == "sim" || string_entrada == "Sim") string_entrada = "1";
                    else if(string_entrada == "nao" || string_entrada == "Nao") string_entrada = "0";
                    arquivo_io << string_entrada << endl;

                    if(string_entrada == "1") consumidor.push_back(new Cliente_Socio);
                    else consumidor.push_back(new Cliente);

                    arquivo_io.close();
                }
            }
            else //Reconhecendo o Cliente cadastrado
            {
                arquivo_io.open("data/clientes/" + string_entrada + ".txt", ios::in | ios::app);

                if(arquivo_io.is_open())
                {

                    while(!arquivo_io.eof())
                    {
                        getline(arquivo_io,linha_arquivo);

                        if(linha_arquivo == "1" || linha_arquivo == "0") break;
                    }

                    arquivo_io.close();
                }

                if(linha_arquivo == "1") consumidor.push_back(new Cliente_Socio);
                else consumidor.push_back(new Cliente);
            }

            consumidor.at(0)->preencher_dados(cpf_entrada);

            system("clear");

            cout << "Seja bem vindo(a), " << consumidor.at(0)->get_nome() << "!" << endl;

            char_entrada = '1';
            estoque = preencher_estoque();

            if(flag_recomendacao == 1)
            {
                carrinho = recomendar(consumidor.at(0)->get_historico(),"preencher");
                flag_recomendacao = 0;
            }

            indice = -1;
            while(indice != 0)
            {
                
                listar_venda();
                
                cout << "\nO que deseja comprar?\n";
                cout << "Digite o indice do produto para adiciona-lo ao carrinho\n";
                cout << "Digite '0' para finalizar a compra\n";
                cout << "\nQUANTIDADE DE PRODUTOS NO CARRINHO:";
                cout << carrinho.size() << endl << endl;

                cout << ": ";
                cin >> indice;

                if(indice > 0)
                {
                    estoque.at(indice-1)--;
                    if(estoque.at(indice-1) < 0)
                    {
                        indice = 0;
                        carrinho.clear();
                        char_entrada = '0';
                        flag_estoque = 1;

                        system("clear");

                        cout << "ESTOQUE INSUFICIENTE\n";
                        cout << "PRESSIONE ENTER PARA VOLTAR AO MENU PRINCIPAL\n";
                        scanf("%*c");
                        scanf("%*c");

                        break;
                    }
                    else
                    {
                        Produto produto;
                        carrinho.push_back(produto);
                        carrinho.back().preencher_dados(indice);
                    }
                }
                system("clear");
            }

            if(flag_estoque == 0)
            {
                scanf("%*c");

                Produto frete("frete");
                frete.set_preco((get_peso_total(carrinho)/3) * 5 * 100); //calculo do frete
                carrinho.push_back(frete);

                cout << "FINALIZANDO A COMPRA" << endl << endl;

                for(int i=0;i<carrinho.size();i++)
                {
                    cout << carrinho.at(i).get_nome();

                    valor = to_string_preco(carrinho.at(i).get_preco());
                    indice = 50 - carrinho.at(i).get_nome().size() - valor.size();
                    while(indice--) cout << ".";

                    cout << valor << endl;
                }
                
                cout << "\nDESCONTO";

                valor = to_string_preco(consumidor.at(0)->calcula_desconto(carrinho));
                indice = 42 - valor.size();
                while(indice--) cout << ".";

                cout << valor << endl;

                cout << "TOTAL";

                valor = to_string_preco(consumidor.at(0)->calcula_preco(carrinho));
                indice = 45 - valor.size();
                while(indice--) cout << ".";

                cout << valor << endl;

                scanf("%*c");

                historico_aux = consumidor.at(0)->get_historico();
                for(int i = 0;i < carrinho.size();i++)
                {
                    editar_quantidade(carrinho.at(i).get_nome(),-1);

                    for(int j = 0;j < carrinho.at(i).get_categorias().size();j++)
                    {
                        historico_aux.at(carrinho.at(i).get_categorias().at(j)-1)++;
                    }
                }
                consumidor.at(0)->set_historico(historico_aux);

                editar_historico(consumidor.at(0)->get_CPF(),consumidor.at(0)->get_historico());
            }

            flag_estoque = 0;
            carrinho.clear();
        }
        if(char_entrada == '4' ) break;
    }

    system("clear");

    return 0;
}

