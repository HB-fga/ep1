#include "metodos.hpp"
#include "produto.hpp"
#include "cliente.hpp"
#include "cliente_socio.hpp"

#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
#include <vector>

char input(void) {

    char entrada;

    system("stty raw");
    std::cin >> entrada;
    system("stty cooked");

    return entrada;
}

std::string to_string_preco(int preco) {

    std::string str_preco = std::to_string(preco);

    switch(str_preco.size())
    {
        case 1:
            str_preco = "0.0" + str_preco;
            break;
        case 2:
            str_preco = "0." + str_preco;
            break;
        default:
            str_preco.insert(str_preco.end()-2,'.');
            break;
    }

    return str_preco;
}

int linhas(std::string path) {

    int l=0;
    std::fstream arquivo;
    std::string linha;
    arquivo.open(path, std::ios::out|std::ios::app);
    arquivo.close();
    arquivo.open(path, std::ios::in);
    while(!arquivo.eof())
    {
        getline(arquivo,linha);
        l++;
    }
    arquivo.close();

    return l;
}

void listar_categorias(void) {

    int indice;
    std::fstream arquivo_io;
    std::string linha_arquivo;

    arquivo_io.open("data/categorias.txt", std::ios::in);

    if(arquivo_io.is_open())
    {
        indice = 0;
        while(!arquivo_io.eof())
        {  
            getline(arquivo_io,linha_arquivo);

            if(indice > 0) std::cout << indice << " " << linha_arquivo << std::endl;

            indice++;
        }
        arquivo_io.close();
    }

    return;
}

std::string lista_de_produtos(void) {

    std::string cmd = "ls data/produtos";
    std::string data;
    FILE * stream;
    const int max_buffer = 256;
    char buffer[max_buffer];
    cmd.append(" 2>&1");

    stream = popen(cmd.c_str(), "r");

    if (stream)
    {
        while (!feof(stream))
        {
        if (fgets(buffer, max_buffer, stream) != NULL) data.append(buffer);
        }
        pclose(stream);
    }
    return data;
}

void listar_produtos(void) {

    std::string linha;
    std::string produto;
    std::string produtos = lista_de_produtos();
    std::vector <std::string> paths;

    std::fstream arquivo;

    int it;

    for(int i=0;i<produtos.size();i++)
    {
        if(produtos.at(i) != '\n')
        {
            produto.insert(produto.end(),produtos.at(i));
        }
        else
        {
            paths.push_back(produto);
            produto = "";
        }
    }

    if(produto != "")
    {
        paths.push_back(produto);
        produto = "";
    }

    std::cout << "================================================" << std::endl;
    
    while(!paths.empty())
    {
        
        arquivo.open("data/produtos/" + paths.back(), std::ios::in);

        if(arquivo.is_open())
        {
            it=0;
            while(!arquivo.eof())
            {
                getline(arquivo,linha);

                if(it == 0) std::cout << "Nome: " << linha << std::endl;
                if(it == 1) std::cout << "Categorias: " << linha << std::endl;
                if(it == 2) std::cout << "Preco(R$): " << linha << std::endl;
                if(it == 3) std::cout << "Peso(kg): " << linha << std::endl;
                if(it == 4) std::cout << "Estoque: " << linha << std::endl;
                if(it >= 5) break;
                it++;
            }
            arquivo.close();
        }

        std::cout << "================================================" << std::endl;

        paths.pop_back();
    }

    return;
}

void listar_venda(void) {

    std::string linha;
    std::string produto;
    std::string produtos = lista_de_produtos();
    std::vector <std::string> paths;

    std::fstream arquivo;

    int it;

    for(int i=0;i<produtos.size();i++)
    {
        if(produtos.at(i) != '\n')
        {
            produto.insert(produto.end(),produtos.at(i));
        }
        else
        {
            paths.push_back(produto);
            produto = "";
        }
    }

    if(produto != "")
    {
        paths.push_back(produto);
        produto = "";
    }

    std::cout << "================================================" << std::endl;

    for(int i=0;i<paths.size();i++)
    {
        
        arquivo.open("data/produtos/" + paths.at(i), std::ios::in);

        if(arquivo.is_open())
        {

            it=0;
            while(!arquivo.eof())
            {
                getline(arquivo,linha);
                
                if(it == 0) std::cout << "[" << i+1 << "]: " << linha << std::endl;
                if(it == 2) std::cout << "Preco(R$): " << linha << std::endl;
                if(it >= 4) break;
                it++;
            }
            arquivo.close();
        }

        std::cout << "================================================" << std::endl;
    }

    return;
}

int numero_de_produtos(void) {

    std::string produto;
    std::string produtos = lista_de_produtos();
    std::vector <std::string> paths;

    for(int i=0;i<produtos.size();i++)
    {
        if(produtos.at(i) != '\n')
        {
            produto.insert(produto.end(),produtos.at(i));
        }
        else
        {
            paths.push_back(produto);
            produto = "";
        }
    }

    if(produto != "")
    {
        paths.push_back(produto);
        produto = "";
    }

    return paths.size();
}

std::vector <int> preencher_estoque(void) {
    
    std::vector <int> estoque;
    Produto produto_aux;
    int n = numero_de_produtos();

    for(int i=1;i<=n;i++)
    {
        produto_aux.preencher_dados(i);
        estoque.push_back(produto_aux.get_quantidade());
    }
    
    return estoque;
}

void editar_quantidade(std::string produto, int num) {

    int indice;
    std::string linha_arquivo;
    std::fstream arquivo_io;

    arquivo_io.open("data/produtos/" + produto + ".txt", std::ios::in | std::ios::out);

    if(arquivo_io.is_open())
    {
        indice = 0;
        while(!arquivo_io.eof())
        {   
            getline(arquivo_io,linha_arquivo);

            indice++;
            if(indice == 5) num += stoi(linha_arquivo);
        }
        
        arquivo_io.close();
    }

    arquivo_io.open("data/produtos/" + produto + ".txt", std::ios::in | std::ios::out);

    if(arquivo_io.is_open())
    {
        indice = 0;
        while(!arquivo_io.eof())
        {   
            getline(arquivo_io,linha_arquivo);

            indice++;
            if(indice == 4) arquivo_io << num << std::endl;
        }
    
        arquivo_io.close();
    }

    return;
}

void editar_historico(std::string cpf, std::vector <int> hist) {

    std::fstream arquivo_io;
    std::string linha_arquivo;
    std::string nova_linha;
    int indice;

    for(int i = 0;i<hist.size();i++)
    {
        nova_linha = nova_linha + std::to_string(hist.at(i));
        nova_linha = nova_linha + " ";

    }

    arquivo_io.open("data/clientes/" + cpf + ".txt", std::ios::in | std::ios::out);

    if(arquivo_io.is_open())
    {

        indice = 0;
        while(!arquivo_io.eof())
        {   
            getline(arquivo_io,linha_arquivo);

            indice++;
            if(indice == 3) arquivo_io << nova_linha << std::endl;
        }
    
        arquivo_io.close();
    }

    return;
}

std::vector <Produto> recomendar(std::vector <int> historico, std::string acao)
{
    int qt;
    int aux;
    int flag;
    int total = 0;
    int maior, maior_p;
    std::vector <int> percentual;
    std::vector <int> aux_vector;
    std::vector <Produto> rec;
    std::vector <Produto> loja;
    std::string preco;

    aux = linhas("data/categorias.txt");

    int histograma[aux] = {};

    for(int i = 0;i<historico.size();i++)
    {
        total += historico.at(i);
    }

    for(int i = 0;i<historico.size();i++)
    {
        qt = (10*historico.at(i))/total;
        while(qt--)
        {
            percentual.push_back(i+1);
            histograma[i+1]++;
        }
    }

    for(int i = 0;i < numero_de_produtos();i++)
    {
        Produto produto;
        loja.push_back(produto);
        loja.back().preencher_dados(i+1);
    }

    // for(int i = 0;i < percentual.size();i++) std::cout << percentual.at(i) << " ";
    // std::cout << "\n";

    aux_vector = percentual;
    percentual.clear();

    maior_p = 1;
    while(maior_p != 0)
    {
        maior = 0;
        maior_p = 0;
        for(int i = 0;i < aux;i++)
        {
            if(histograma[i] > maior)
            {
                maior = histograma[i];
                maior_p = i;
            }
        }

        while(maior--) percentual.push_back(maior_p);

        histograma[maior_p]=0;
    }

    for(int i = 0;i < percentual.size();i++)
    {
        flag = 0;
        for(int j = 0;j < loja.size();j++)
        {
            for(int k = 0;k < loja.at(j).get_categorias().size();k++)
            {
                if(percentual.at(i) == loja.at(j).get_categorias().at(k) && loja.at(j).get_quantidade() > 0)
                {
                    rec.push_back(loja.at(j));
                    loja.erase(loja.begin() + j);
                    flag = 1;
                }
            }
            if(flag == 1) break;
        }
    }

    if(acao == "listar") // apenas lista
    {
        std::cout << "================================================" << std::endl;

        for(int i = 0;i < rec.size();i++)
        {
            preco = to_string_preco(rec.at(i).get_preco());

            std::cout << rec.at(i).get_nome() << std::endl;
            std::cout << "Preco: " << preco << std::endl;

            std::cout << "================================================" << std::endl;
        }

        rec.clear();
    }
    
    return rec;
}

float get_peso_total(std::vector <Produto> carrinho) {

    float peso_total = 0.0;

    for(int i = 0;i < carrinho.size();i++)
    {
        peso_total += carrinho.at(i).get_peso();
    }

    return peso_total;
}

