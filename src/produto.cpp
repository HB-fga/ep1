#include "metodos.hpp"
#include "produto.hpp"
#include "cliente.hpp"
#include "cliente_socio.hpp"

#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
#include <vector>

Produto::Produto()
{
    nome = "Generico";
    //categorias inicializa vazio por padrao
    preco = 0;
    quantidade = 0;
    peso = 0.0;
}
Produto::Produto(std::string frete)
{
    if(frete == "frete")
    {
        nome = frete;
        preco = 0;
        quantidade = 0;
        peso = 0.0;
    }
    else
    {
        nome = "Generico";
        preco = 0;
        quantidade = 0;
        peso = 0.0;
    }
    
}
Produto::~Produto()
{

}
std::string Produto::get_nome()
{
    return nome;
}
void Produto::set_nome(std::string nome)
{
    this->nome = nome;
}
std::vector <int> Produto::get_categorias()
{
    return categorias;
}
void Produto::set_categorias(std::vector <int> categorias)
{
    this->categorias = categorias;
}
int Produto::get_preco()
{
    return preco;
}
void Produto::set_preco(int preco)
{
    this->preco = preco;
}
float Produto::get_peso()
{
    return peso;
}
void Produto::set_peso(float peso)
{
    this->peso = peso;
}
int Produto::get_quantidade()
{
    return quantidade;
}
void Produto::set_quantidade(int quantidade)
{
    this->quantidade = quantidade;
}
void Produto::preencher_dados(int indice)
{
    indice--;

    std::string linha;
    std::string palavra;
    std::string produtos = lista_de_produtos();
    std::vector <std::string> paths;

    std::fstream arquivo;

    int it;

    for(int i=0;i<produtos.size();i++)
    {
        if(produtos.at(i) != '\n')
        {
            palavra.insert(palavra.end(),produtos.at(i));
        }
        else
        {
            paths.push_back(palavra);
            palavra = "";
        }
    }

    if(palavra != "")
    {
        paths.push_back(palavra);
        palavra = "";
    }

    arquivo.open("data/produtos/" + paths.at(indice), std::ios::in);

    if(arquivo.is_open())
    {
        it=0;
        while(!arquivo.eof())
        {
            getline(arquivo,linha);

            if(it == 0) this->nome = linha;
            if(it == 1)
            {
                for(int j=0;j<linha.size();j++)
                {
                    if(linha.at(j) != ' ')
                    {
                        palavra.insert(palavra.end(),linha.at(j));
                    }
                    
                    if(linha.at(j) == ' ' || j+1 == linha.size())
                    {
                        categorias.push_back(stoi(palavra));
                        palavra = "";
                    }
                }
            }
            if(it == 2) this->preco = (stod(linha)*100);
            if(it == 3) this->peso = stof(linha);
            if(it == 4) this->quantidade = stoi(linha);
            if(it >= 5) break;

            it++;
        }
        arquivo.close();
    }
}